Chrome extension that generates a random-ish User-Agent for incognito tabs.

Why? Well .. paranoia.

This was a bit of a nightmare to get working, but I finally found the
required invocation magic.
