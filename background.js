let os_list = [
  'Windows NT 5.1; Win64; x64',
  'Windows NT 6.1; Win64; x64',
  'Windows NT 6.2; Win64; x64',
  'Windows NT 6.3; Win64; x64',
  'Windows NT 10.0; Win64; x64',
];

chrome.webRequest.onBeforeSendHeaders.addListener(
  details => {
    chrome.tabs.get(details.tabId, tab => {
      if (tab.incognito) {
        for (let i = 0; i < details.requestHeaders.length; i++) {
          if (details.requestHeaders[i].name === 'User-Agent') {
            let os = os_list[Math.floor(Math.random() * os_list.length)],
                version = [
                  60 + Math.floor(Math.random() * 8),
                  0,
                  1000 + Math.floor(Math.random() * 8000),
                  10 + Math.floor(Math.random() * 90)
                ].join('.');
            details.requestHeaders[i].value = `Mozilla/5.0 (${os}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/${version} Safari/537.36`;
            console.log('agentify: ' + details.requestHeaders[i].value);
            break;
          }
        }
      }
      return { requestHeaders: details.requestHeaders };
    });
  },
  { urls: [ "<all_urls>" ] },
  [ "blocking", "requestHeaders" ]
);
